note
	description: "eiffel-libsodium application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

	LIBSODIUM_SHARED

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			if crypt.init_successful then
				print ("Crypto initialised OK !%N")
			end
			print_constants
			test_2
		end

	test_message: STRING_8 = "This is a test message ..."

	test_2
		local
			l_string: STRING_8
			l_msg: SODIUM_MESSAGE
			l_key: SODIUM_KEY
			l_nonce: SODIUM_NONCE
			l_encrypted: SODIUM_ENCRYPTED
			l_decrypted: SODIUM_MESSAGE
			l_success: BOOLEAN
		do
			l_string := test_message
				-- Create message from test string
			create l_msg.make_from_string (l_string)

				-- Create and initialize key
			create l_key

				-- Create and initialize nonce
			create l_nonce

				-- Create encrypted message ...
			create l_encrypted.make_from_message (l_msg, l_nonce, l_key)

				-- (Re)Create the decrypted message
			create l_decrypted.make_from_encrypted (l_encrypted, l_nonce, l_key)

				-- Compare message with encrypted/decrypted message ...
			l_success := crypt.compare (l_decrypted, l_msg)

				-- Report on the success/failure of this test ...
			if l_success then
				l_string := "Encrypt/Decrypt test succeeded ...%N"
			else
				l_string := "Encrypt/Decrypt test failed ...%N"
			end
			print (l_string)

		end

	test
		local
			l_string: STRING_8
			l_msg: MANAGED_POINTER
			l_key: MANAGED_POINTER
			l_nonce: MANAGED_POINTER
			l_encrypted: MANAGED_POINTER
			l_decrypted: MANAGED_POINTER
			l_success: BOOLEAN
		do
			l_string := test_message
				-- Generate message buffer from test string
			create l_msg.make_from_pointer (l_string.area.base_address, l_string.count)

				-- Allocate key buffer
			create l_key.make (crypt.secretbox_keybytes)
				-- Generate (secret) key into key buffer
			crypt.secretbox_keygen (l_key)

				-- Allocate nonce buffer
			create l_nonce.make (crypt.secretbox_noncebytes)
				-- Generate nonce
			crypt.randombytes_buf (l_nonce.item, l_nonce.count)
				--	crypt.randombytes_buf (l_nonce) -- TODO

				-- Allocate encrypted message buffer
			create l_encrypted.make (l_string.count + crypt.secretbox_macbytes)
				-- Encrypt 'message' using key and nonce into encrypted buffer
			crypt.secretbox_easy (l_encrypted, l_msg, l_nonce, l_key)

				-- Allocate decrypted message buffer
			create l_decrypted.make (l_string.count)
				-- Decrypt into above buffer
			crypt.secretbox_open_easy (l_decrypted, l_encrypted, l_nonce, l_key)

				-- Compare message with encrypted/decrypted message ...
			l_success := crypt.compare (l_decrypted, l_msg)
				--	l_success := crypt.compare (l_msg, l_msg)

				-- Report on the success/failure of this test ...
			if l_success then
				l_string := "Encrypt/Decrypt test succeeded ...%N"
			else
				l_string := "Encrypt/Decrypt test failed ...%N"
			end
			print (l_string)

		end

	print_constants
		do
			print ("crypt.secretbox_keybytes: ")
			print (crypt.secretbox_keybytes.out)
			print ("%N")
			print ("crypt.secretbox_noncebytes: ")
			print (crypt.secretbox_noncebytes.out)
			print ("%N")
			print ("crypt.secretbox_macbytes: ")
			print (crypt.secretbox_macbytes.out)
			print ("%N")

				-- encryption secret stream constants ...

			print ("crypt.secretstream_xchacha20poly1305_ABYTES: ")
			print (crypt.secretstream_xchacha20poly1305_ABYTES.out)
			print ("%N")
			print ("crypt.secretstream_xchacha20poly1305_KEYBYTES: ")
			print (crypt.secretstream_xchacha20poly1305_KEYBYTES.out)
			print ("%N")
			print ("crypt.secretstream_xchacha20poly1305_MESSAGEBYTES_MAX: ")
			print (crypt.secretstream_xchacha20poly1305_MESSAGEBYTES_MAX.out)
			print ("%N")
			print ("crypt.secretstream_xchacha20poly1305_TAG_MESSAGE: ")
			print (crypt.secretstream_xchacha20poly1305_TAG_MESSAGE.out)
			print ("%N")
			print ("crypt.secretstream_xchacha20poly1305_TAG_PUSH: ")
			print (crypt.secretstream_xchacha20poly1305_TAG_PUSH.out)
			print ("%N")
			print ("crypt.secretstream_xchacha20poly1305_TAG_REKEY: ")
			print (crypt.secretstream_xchacha20poly1305_TAG_REKEY.out)
			print ("%N")
			print ("crypt.secretstream_xchacha20poly1305_TAG_FINAL: ")
			print (crypt.secretstream_xchacha20poly1305_TAG_FINAL.out)
			print ("%N")

		end

feature

end
