note
	description: "Summary description for {SODIUM_KEY}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	SODIUM_KEY

inherit
	LIBSODIUM
		undefine is_equal, copy, default_create end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		undefine default_create
		end
create
	default_create
feature
	default_create
		do
			make (secretbox_keybytes)
			secretbox_keygen (Current)
		end

end
