note
	description: "Summary description for {SODIUM_ENCRYPTED}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	SODIUM_ENCRYPTED
inherit
	LIBSODIUM
		undefine is_equal, copy end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		end
create
	make_from_message

feature {NONE} --Initialization

	make_from_message (a_message: SODIUM_MESSAGE; a_nonce: SODIUM_NONCE; a_key: SODIUM_KEY)
		do
			make (a_message.count + secretbox_macbytes)
			secretbox_easy (Current, a_message, a_nonce, a_key)
		end


end
