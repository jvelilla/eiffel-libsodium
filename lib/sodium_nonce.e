note
	description: "Summary description for {SODIUM_NONCE}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"
	TODO: "[
		Derive, possibly more quickly than calling randombytes, a successor
		nonce from the current one, either by incrementation or by seeding
		a RNG from the initial random value.
	]"

class
	SODIUM_NONCE

inherit
	LIBSODIUM
		undefine is_equal, copy, default_create end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		undefine default_create
		end
create
	default_create

feature

	default_create
		do
			make (secretbox_noncebytes)
			randomize
		end

	randomize
		do
			randombytes_buf (item, count)
		end

end
