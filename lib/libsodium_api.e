note
	description: "API to libsodium 'C' interface"
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	LIBSODIUM_API

feature -- Initialisation

	sodium_init: INTEGER
		external "C inline use <sodium.h>"
		alias "sodium_init()" end

feature -- Public/Private key encryption/authentication

	crypto_box_publickeybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_box_PUBLICKEYBYTES " end

	crypto_box_secretkeybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_box_SECRETKEYBYTES " end

	crypto_box_macbytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_box_MACBYTES " end

	crypto_box_keypair (a_publickey_ptr, a_privatekey_ptr: POINTER)
		external "C inline use <sodium.h>"
		alias "crypto_box_keypair " end

feature -- Utility

		--	sodium_memcmp

	sodium_bin2hex (a_hex: POINTER; a_hex_max: INTEGER_64; a_bin: POINTER; a_bin_size: INTEGER_64)
		external "C use <sodium.h>"
		end

		--	sodium_hex2bin
		--	sodium_bin2base64
		--	sodium_base642bin
		--	sodium_increment
		--	sodium_add

	sodium_compare (a_ptr, b_ptr: POINTER; a_size: INTEGER_64): INTEGER
		external "C use <sodium.h>"
				--		alias "sodium_compare(void *, void *, size_t)"
		end

		--	sodium_is_zero
feature -- Random

	randombytes_random: NATURAL_32
		external "C inline use <sodium.h>"
		end

	randombytes_uniform (upper_bound: NATURAL_32): NATURAL_32
		external "C inline use <sodium.h>"

		end

	randombytes_buf (buf: POINTER; size: INTEGER_64)
		external "C inline use <sodium.h>"
		end

	randombytes_buf_deterministic (buf: POINTER; size: INTEGER_64; a_seed: POINTER)
		external "C inline use <sodium.h>"
		end

	randombytes_seedbytes: INTEGER
		external "C inline use <sodium.h>"
		alias "randombytes_SEEDBYTES " end

feature -- Secret Box

		-- Constatnts
	secretbox_keybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_KEYBYTES " end

	secretbox_macbytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_MACBYTES " end

	secretbox_noncebytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_NONCEBYTES " end

		-- Routines
	crypto_secretbox_keygen (a_ptr: POINTER)
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_keygen " end

	crypto_secretbox_easy (a_encrypted_ptr, a_msg_ptr: POINTER; a_msg_size: INTEGER_64; a_nonce_ptr, a_key_ptr: POINTER): INTEGER
		external "C use <sodium.h>"
		alias "crypto_secretbox_easy " end

	crypto_secretbox_open_easy (a_decrypted_ptr, a_encrypted_ptr: POINTER; a_encrypted_size: INTEGER_64; a_nonce_ptr, a_key_ptr: POINTER): INTEGER
		external "C use <sodium.h>"
		alias "crypto_secretbox_open_easy " end

feature -- constants re encryption stream

	secretstream_xchacha20poly1305_ABYTES: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_ABYTES " end

	secretstream_xchacha20poly1305_KEYBYTES: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_KEYBYTES " end

	secretstream_xchacha20poly1305_MESSAGEBYTES_MAX: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_MESSAGEBYTES_MAX " end

	secretstream_xchacha20poly1305_TAG_MESSAGE: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_MESSAGE " end

	secretstream_xchacha20poly1305_TAG_PUSH: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_PUSH " end

	secretstream_xchacha20poly1305_TAG_REKEY: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_REKEY " end

	secretstream_xchacha20poly1305_TAG_FINAL: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_FINAL " end

end
